import React from "react";
import Product from "../Product";

export type ProductListProps = {
  products: any;
};
function ProductList({ products }: ProductListProps) {
  return (
    <div className="flex flex-wrap gap-y-4 gap-x-2 justify-center py-4">
      {products.map((product: any, i: number) => {
        return <Product key={i} product={product} />;
      })}
    </div>
  );
}

export default ProductList;
