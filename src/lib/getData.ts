export async function getData(endpoint: string) {
  try {
    const response = await fetch(endpoint, {
      cache: "no-store",
    });
    const data = await response.json();
    console.log("getData>>>", data);
    return data;
  } catch (error) {
    console.log("getData", error);
  }
}
